$tokenbot = $Hook['env']['token_a_bot']; 
$endpoint = "https://api.telegram.org/bot".$tokenbot."/";

$messages = $Hook["params"];
if (isset($messages['message'])) {
  $messages = $messages['message'];
} else {
  echo 'URL hook.io: https://'.$Hook['input']['host'].$Hook['input']['path'];
}

if (isset($messages['new_chat_member'])) {
    $pesan = "Hai ".$messages['new_chat_member']['first_name']." ! 😊\n";
    $pesan.= "Selamat datang di Grup ".$messages['chat']['title']."\n";
    $pesan.= indonesian_date(time());
  kirimPesan($messages['chat']['id'], $messages['message_id'], $pesan);
}

if (isset($messages['left_chat_member'])) {
    $pesan = "Sampai jumpa lagi ya ".$messages['left_chat_member']['first_name'] ;
    kirimPesan($messages['chat']['id'],$messages['message_id'],  $pesan);
} 

if(isset($messages['text'])){
   $textur = preg_replace('/\s\s+/', ' ', $messages['text']); 
   $command = explode(' ',$textur,2); 
	switch ($command[0]){
      case '/start':       
        $pesan = "Hai Selamat Datang ! ".$messages['chat']['first_name']." 😊\n";
        $pesan.= indonesian_date(time())."\n";
        $pesan.= "Silahkan gunakan command berikut : \n";
        $pesan.= "1. /start \n2. /logo \n3. /about";
        kirimPesan($messages['chat']['id'], $messages['message_id'], $pesan);
      break;
      case '/logo':
        kirimGambar($messages['chat']['id'], 'https://www.unuja.ac.id/logo-unuja.png');
      break;
      case '/about':
        $pesan = "Ini cuma Bot uji coba ya ! 😊 \n" ;
        $pesan.= "Silahkan kunjungi www.unuja.ac.id";
        kirimPesan($messages['chat']['id'], $messages['message_id'], $pesan);
      break;
      case '/akademik':
        $pesan = "Ex Command = /akademik dosen \n" ;
        kirimPesan($messages['chat']['id'], $messages['message_id'], $pesan);
      break;
    }
  
}


function kirimGambar($chat_id, $url)
{
    global $endpoint;
    $data = array(
        'chat_id' => $chat_id,
        'photo'  => $url,
      	'caption' => 'Unuja Keren'
    );
 
  	// Buat parameter pengiriman
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data),
        ),
    );
    $context  = stream_context_create($options); 
    $result = file_get_contents($endpoint.'sendPhoto', false, $context);
   
    return $result;
}

function kirimPesan($chat_id, $msgid, $text)
{
    global $endpoint;
    $data = array(
        'chat_id' => $chat_id,
        'text'  => $text
    );
 
  	// Buat parameter pengiriman
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data),
        ),
    );
    $context  = stream_context_create($options); 
    $result = file_get_contents($endpoint.'sendMessage', false, $context);
   
    return $result;
}

function indonesian_date ($timestamp = '', $date_format = 'l, j F Y | H:i', $suffix = 'WIB') {
    if (trim ($timestamp) == '')
    {
            $timestamp = time ();
    }
    elseif (!ctype_digit ($timestamp))
    {
        $timestamp = strtotime ($timestamp);
    }
    # remove S (st,nd,rd,th) there are no such things in indonesia :p
    $date_format = preg_replace ("/S/", "", $date_format);
    $pattern = array (
        '/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
        '/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
        '/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
        '/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
        '/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
        '/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
        '/April/','/June/','/July/','/August/','/September/','/October/',
        '/November/','/December/',
    );
    $replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
        'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
        'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
        'Januari','Februari','Maret','April','Juni','Juli','Agustus','Sepember',
        'Oktober','November','Desember',
    );
    $date = date ($date_format, $timestamp);
    $date = preg_replace ($pattern, $replace, $date);
    $date = "{$date} {$suffix}";
    return $date;
} 